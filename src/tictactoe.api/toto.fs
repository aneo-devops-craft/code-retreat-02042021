﻿namespace toto

open System.Collections
open System.Collections.Generic

type Res<'a> = 
| Ok of 'a
| Error of string

module Res =
    let map f r = 
        match r with 
        | Ok d -> Ok(f(d))
        | Error err -> Error err

    let bind f r = 
        match r with 
        | Ok d -> f(d)
        | Error f -> Error f

module Service =

    type Stack(list:List<string>) =
        member this.pop () =
            list.[0]

        interface IEnumerable<string> with  
            member this.GetEnumerator() =
                list.GetEnumerator() :> IEnumerator

            member this.GetEnumerator() =
                list.GetEnumerator() :> IEnumerator<string>

    //class Stack {
    //   private string[] _list;

    //    public Stack(string[] list) {
    //        _list = list;
    //    }

    //    public string pop() {
    //        return _list[0]
    //    }
    //}

    let (<!>) r f = Res.map f r
    let (>>=) r f = Res.bind f r

    let retn x = Ok x

    type Item = {
        Id: string
        Value: int
    }

    let private getFromDb id : Res<Item> =
        if id = "plop" then 
            Error "PLOP"
        else 
            Ok {
                Id = id
                Value = 1
            }

    let private validate (item:Item) : Res<Item> =
        if (item.Value < 1) then 
            Error "wrong item"
        else
            Ok item

    let private getValueInPercent (item:Item) =
        item.Value / 100

    let getItemValueInPercent (id:string) =
         id 
         |> getFromDb 
         >>= validate
         <!> getValueInPercent

    let add x v =
        x + v

    let main() =
        let r = getItemValueInPercent "id"
        match r with
        | Ok o -> printfn "%i" o
        | Error err -> failwith err