﻿namespace Game 

module TicTacToe = 
    type Player =
    | X
    | O

    type Winner =
    | Player of Player
    | Nobody 
    
    module Winner =
        let hasPlayerWon w = not(w = Nobody)
        let singleWinnerOrNobody list = 
            if Seq.isEmpty list then
                Nobody
            else
                Seq.head list

    type Cell =
    | OccupiedBy of Player
    | Empty

    module Cell =
        let isOccupiedByPlayer player cell = 
            let expectedCell = Cell.OccupiedBy player
            cell = expectedCell

    type Line = Cell seq

    module Line = 

        let occupiedBy player line =
            Seq.forall (Cell.isOccupiedByPlayer player) line

        let (|IsOccupiedBy|_|) player line =
            if occupiedBy player line then 
                Some ()
            else 
                None

        let hasWinner line =
            match line with 
            | IsOccupiedBy X -> Player X
            | IsOccupiedBy O -> Player O
            | _ -> Nobody
    
    type Board = Line seq

    module Board =
        let init : Board =
            seq [|[|Empty|]|]

        let hasWinnerByLine (board:Board) = 
            board |> (Seq.map Line.hasWinner 
                      >> Seq.filter Winner.hasPlayerWon 
                      >> Winner.singleWinnerOrNobody)


